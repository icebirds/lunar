# 农历转换算法


> Copyright 2017 Icebird Yin.  
> Released under MIT Lisence

中国农历并非简单的太阳历或者太阴历，其规则中包含几个重要部分：

1. 太阴月历
2. 接近太阳年的年历
3. 24节气
4. 基于太阳历的干支纪年、月、日、时历法

基于以上原因，这个类并没有使用传统的lunar做为名称，而是使用了更为精确的CnCal（Chinese Calendar）做为类名。

> 历法的来源是基于天文观测，全球的历法主要分为太阳历、太阴历和阴阳历三种。太阳历是以地球绕太阳公转周期为基准的历法，目前我们使用的公历就是典型的太阳历。而太阴历则是以月亮的公转周期为基准的历法，伊斯兰历就是典型的太阴历。而阴阳历则是同时兼顾太阳和月亮两者的公转周期制定的历法，中国的农历就是典型的阴阳历。

以下就是实现它的算法：

## 农历日期类

先实现一个简单的农历日期类

	class CnDate

在创建一个农历日期类的时候，要提供一个公历的日期，以便获取对应的农历日期

提供的公历日期为一个Date实例，如果留空，则自动获取当前时间：

		constructor : (date = new Date()) ->
			this.date = date

### 获取对应的农历日期

		getCnDate: () ->
			

### 获取对应的干支

		getGanzhi: () ->
			

### 获得年份干支

> 干支历每年的新年都是从立春开始的，如果不到立春，则以上一年公历的年数计算干支
> 根据简单查找，公元4年是甲子年，因此简单的年份计算方法如下：
> 年干： (年份 - 4) % 10
> 年支： (年份 - 4) % 12
> 如年干和年支取余所得数字为负数，则需分别加10和12后方可返回
> 返回结果如下：
> {
> 	index:{0,0},  //表示干支数字
> 	en:"jia zi", //表示英文干支
> 	zh:"甲子" //表示中文干支
> }

		getGanzhiYear: () ->
			enGan = ['jia','yi','bing','ding','wu','ji','geng','xin','ren','gui']
			enZhi = ['zi', 'chou', 'yin', 'mao', 'chen', 'si', "wu", 'wei', 'shen', 'you', 'xu', 'hai']
			zhGan = ['甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸']
			zhZhi = ['子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥' ]
			thisYear = this.date.getFullYear();
			gan = ( thisYear - 4 ) % 10
			gan += 10 if gan < 0
			zhi = ( thisYear - 4 ) % 12
			zhi += 12 if zhi < 0
			console.log gan , zhi, enGan[gan] + " " + enZhi[zhi], zhGan[gan]+zhZhi[zhi]

静态方法：获取某年的节气

		this.getSolarTerm = (year) ->

静态方法：将农历转换为公历

		this.cnDate2solar = (lunarDate) ->
			

	window.CnDate = CnDate
